# VideoIdent SDK Android


The VideoIdentSDK allows you to use the VideoIdent Service within your own app.

## Setup

Add the following dependency to your build.gradle file.

```
implementation "ag.pos.ident:videoident-sdk:$version"
```


## Configurations

The main entrypoint is the `VideoIdentSdk` class, an instance can be accessed via the `getInstance` method.
 
In order to use the SDK, you need a server url, and the target name for your identifications. 

These configurations are supplied via the `configure` method and the returned builder.

We provide an Builder class to easily configure and initialize the SDK.  

See `VideoIdentSdk.Builder` class for more infos.


The VideoIdentConfiguration can set a color scheme and logo. 

See `VideoIdentConfiguration` class for additional info on how to provide these settings.

Styling of the UI is done via Android Themes. They must extend the `VideoIdentTheme` provided with the SDK.

Example style:

``` xml  
 <style name="VideoIdentTheme.Identification" parent="VideoIdentTheme">
    <item name="colorPrimary">#450</item>
    <item name="colorPrimaryDark">#000</item>
    <item name="colorAccent">#0ff</item>
</style>
```

`colorPrimary` changes the highlight color of Primary Elements (i.e Button Text)
`colorPrimaryDark` changes the color of the statusBar 
`colorAccent` changes the accent collor of UI elements(i.e Button Outline, Checkboxes Loading Spinner,)

## Strings

To override the provided strings you can add the following strings to your string.xml files.
```
<string name="video_ident_intro_legal_text">Name beglaubigt Ihre Personenidentifikat... </string>
<string name="video_ident_rejected_title"></string>
<string name="video_ident_rejected_message">Vielen Dank für die Durchführung der Video–Legitimierung....</string>

<string name="video_ident_accepted_title"></string>
<string name="video_ident_accepted_message">Legitimierung erfolgreich abgeschlossen.</string>

<string name="video_ident_error_title"></string>
<string name="video_ident_error_message">Es ist ein Problem aufgetreten.</string>

<string name="video_ident_status_independent_title">Legitimierung abgeschlossen</string>
<string name="video_ident_status_independent_message">Sie erhalten in Kürze eine E-Mail mit weiteren Informationen.</string>
```


## Classes

Description of the public API classes of the Android SDK.


## `VideoIdentSDK`

### Methods

#### `getInstance()`

Returns Singleton Instance of the SDK.

```java
static getInstance()
```


#### `configure()`

Returns Builder Instance to Configure the SDK.

```java
public VideoIdentSDK.Builder configure(String url)
```


**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| url | String                           | The server url the Sdk connects to. This will be providet to you by POS   |


Urls usually must end with "/" and the path usually end with "/POSIdent/".

-------

#### `startIdentification()`

Starts the identification process.

```java
public void startIdentification(String identificaitonGuid, Context c ,VideoIdentConfiguration config )
public void startIdentification(String identificaitonGuid, Context c)
```


**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| identificaitonGuid | String                           | Guid of the prepared identification   |
| context | Context                           | Context to be uses by the SDK during identification   |
| config | VideoIdentConfiguration  (optional)                         | Configuration of the stating Identification If an configuration is supplied it will override the default configuration  |


If the configuration is invalid or the SDK has not been configured at all, an exception will be thrown.



---

#### `cancelIdentification()`

Cancels an currently running identification process

```java
public void cancelIdentification()
```

If no identificaiton is in process, the method does nothing.


---





#### `addIdentificationListener()`

Adds a listerner wich will be notified of the outcome of the identification or errors.

```java
public void addIdentificationListener(@NotNull VideoIdentListener videoIdentListener)
```

**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| videoIdentListener | VideoIdentListener                           | Listener for Identification results.  |


---



#### `removeIdentificationListener()`

Removes an listener from the SDK. If null is passed as parameter, all listeners will be removed.

```java
public void removeIdentificationListener(VideoIdentListener videoIdentListener)
```

**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| videoIdentListener | VideoIdentListener                           | Listener for Identification results.  |


----

#### `requestPermissionHelper(activity , code)`

Helper function to show Runtime Permission request. Result is returned in activities `onRequestPermissionsResult` method.

```java
public void requestPermissionHelper(Activity activity, int code)
```

**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| activity | Activity                           | Activity to show permission Request  |
| code | int                           | request code to identify teh request |

----




## `VideoIdentSDK.Builder`

Builder to generate valid configuration. The Builder is created by calling `configure` on the VideoIdentSDK.


### Methods


#### `setConfiguration()`

Sets the default configuration of the SDK. VideoIdentConfigurations are created via the `VideoIdentConfiguration.Builder`. 

```java
public Builder setConfiguration(@NotNull VideoIdentConfiguration config)
```

**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| config | VideoIdentConfiguration                           | Configuarion for the SDK .  |


#### `setLogLevel()`

Sets the current log level for the SDK. Default: 0 (Disabled).

```java
public void setLogLevel(int level)
```

**Parameters:**

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| level | Int                           | Desired log level to be used.  |


Integer constants from `android.util.Log` are used.

---

#### `initialize()`

Initialized the SDK. This checks the validity of the existing configuration, and throws an exception if the configuration is not valid.

```java
public void initialize()
```


## `VideoIdentConfiguration`


A `VideoIdentConfiguration` is created via the `create` method and might contain a logo and a Theme to customize the UI.  

### Methods

#### `create()`

Creates an `VideoIdentConfiguration.Builder` to build the VideoIdentConfiguration.

```java
static create(int name)
```

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| name | int                           | Android String Resource Id of the configuration name  |


----

## `VideoIdentConfiguration.Builder`

### Methods

#### `setImage()`

Sets the Image to be shown during the Identification.

```java
public VideoIdentConfiguration.Builder setImage(int logo)
```

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| logo | int                           | Android Drawable Resource Id of the desired logo image  |


#### `setTheme()`

Sets the Theme of colors used during the Identification.

```java
public VideoIdentConfiguration.Builder setTheme(int theme)
```

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| theme | int                           | Android Style Resource Id of the desired theme  |


#### `skipUserDataEntry()`

Skip user data entry screen if data is already present. Will throw an error otherwise. If this flag is set the Camera and Audio runtime permissions must be granted before the stat of the sdk.
`requestPermissionHelper` method can be used to request the required permissions.

```java
public VideoIdentConfiguration.Builder skipUserDataEntry(boolean skipUserDataEntry = false)
```

| Name                                                   | Type                             | Description                                                             |
| ------------------------------------------------------ | -------------------------------- | ----------------------------------------------------------------------- |
| skipUserDataEntry | boolean                           |  boolean to allow skipUserData  |


#### `build()`

Creates the `VideoIdentConfiguration` object from the provided data.

```java
public VideoIdentConfiguration build() 
```

----

## `VideoIdentListener`

### Methods

The VideoIdentListener provides some callbacks when the identification process has ended. 

#### `onIdentificationComplete()`
```java
void onIdentificationComplete(IdentificationStatus state);
```

This method will be called when the identification process has ended and the corresponding Response is passed as parameter. See `IdentificationStatus` for details.




#### `onIdentificationError()`

```java
void onIdentificationError(Exception error);
```


This method will be called when an error during the identification process has occured. 

#### `onIdentificationCanceled()`

```java
void onIdentificationCanceled();
```

This method will be called when the identification process has been canceled, either by the user or by the app. 



## `IdentificationResponse`

Response returned by the `onIdentificationComplete` callback. This contains the status of the identification and the redirect url if the user is beeing redirected.


## `IdentificationStatus`

Enumeration of possible Identification End States
```
enum IdentificationStatus {
    Redirect,
    Accepted,
    Rejected,
    Error,
    StatusIndependent
}
```

# Example 

Example integration of the SDK.

```java
VideoIdentConfiguration config = VideoIdentConfiguration.create(R.string.app_name)
        .setImage(R.drawable.ic_logo)
        .setTheme(R.style.VideoIdentTheme_Identification)
        .build();

VideoIdentConfiguration alternateConfig = VideoIdentConfiguration.create(R.string.name)
        .setImage(R.drawable.ic_logo_alt)
        .setTheme(R.style.VideoIdentTheme_Identification_Alt)
        .skipUserDataEntry()
        .build();


VideoIdentSdk.getInstance().configure("https://server/POSIdent/")
        .setConfiguration(config)
        .setLogLevel(Log.VERBOSE)
        .initialize();

VideoIdentListener listener =  new VideoIdentListener() {
    @Override
    public void onIdentificationComplete(IdentificationResponse response) {
        VideoIdentSdk.getInstance().removeIdentificationListener(this);
        System.out.println("Identification finished with status "+response.status);
    }

    @Override
    public void onIdentificationError(Exception error) {
        System.out.println("An Error occurred during the Identification");
    }

    @Override
    public void onIdentificationCanceled() {
        System.out.println("The Identification has been Canceled");
    }
};

VideoIdentSdk.getInstance().addIdentificationListener(listener);
VideoIdentSdk.getInstance().startIdentification("GUID", this , alternateConfig);

```