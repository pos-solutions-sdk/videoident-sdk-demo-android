package ag.pos.videoident.demo

import ag.pos.videoident.api.PermissionMissingException
import ag.pos.videoident.*
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    val identificatioGuid ="" // TODO USE ACTUAL GUID OR ENTETR WITHIN APP
    val URL ="https://yourserver/POSident/" //TODO use Posident Server

    val skipUserData = true
    val REQUEST_CODE_PERMISSION = 9078

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<EditText>(R.id.guid).setText(identificatioGuid)

        VideoIdentSdk.getInstance()
            .configure(URL)
            .setConfiguration(VideoIdentConfiguration.create().setImage(R.drawable.logo_brand).build())
            .initialize()


        VideoIdentSdk.getInstance().addIdentificationListener( object  : VideoIdentListener {
            override fun onIdentificationComplete(response: IdentificationResponse?) {
                VideoIdentSdk.getInstance().cancelIdentification() // close SDK once identification is finished.
                Toast.makeText(this@MainActivity, "Completed with Status: "+ response?.status, Toast.LENGTH_LONG).show()
            }

            override fun onIdentificationError(error: Exception?) {
                Toast.makeText(this@MainActivity, "Error: "+ error?.message, Toast.LENGTH_LONG).show()
                if(error is PermissionMissingException){
                    VideoIdentSdk.getInstance().requestPermissionHelper(this@MainActivity, REQUEST_CODE_PERMISSION )
                }
            }

            override fun onIdentificationCanceled() {
                Toast.makeText(this@MainActivity, "Canceled", Toast.LENGTH_LONG).show()
            }
        })

        findViewById<View>(R.id.button).setOnClickListener { view ->

            VideoIdentSdk.getInstance().startIdentification(findViewById<EditText>(R.id.guid).text.toString(), view.context)
        }



        findViewById<View>(R.id.button_alt).setOnClickListener { view ->
            startIdentificationAlt()
        }

    }

    fun startIdentificationAlt(){
        VideoIdentSdk.getInstance().startIdentification(findViewById<EditText>(R.id.guid).text.toString(), this ,
            VideoIdentConfiguration.create()
                .setImage(R.drawable.ic_android)
                .setTheme(R.style.VideoIdentTheme_Alt)
                .skipUserDataEntry(skipUserData)
                .build())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_PERMISSION -> {
                if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                    startIdentificationAlt()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }


}